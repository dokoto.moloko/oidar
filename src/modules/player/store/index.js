import { listStations } from '@/common/helpers/api/stations';
import { LIST_STATIONS_OK, LIST_STATIONS_ERROR } from './types';

const state = {
  stations: [],
  error: null,
};

const actions = {
  async doListStations(ctx) {
    try {
      const { token } = ctx.rootGetters['auth/session'];
      const { radios } = await listStations(token);
      ctx.commit(LIST_STATIONS_OK, radios);
      return radios;
    } catch (error) {
      ctx.commit(LIST_STATIONS_ERROR, error);
      throw error;
    }
  },
};

const mutations = {
  [LIST_STATIONS_OK](state, stations) {
    state.stations = stations;
  },
  [LIST_STATIONS_ERROR](state, error) {
    state.error = error;
  },
};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
