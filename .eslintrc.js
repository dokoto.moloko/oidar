module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/max-attributes-per-line': 0,
    'vue/html-closing-bracket-newline': 0,
    'no-shadow': 0,
    'arrow-parens': 0,
    'object-shorthand': 0,
    'func-names': 0,
    'import/prefer-default-export': 0,
    'object-curly-newline': 0,
    'import/no-extraneous-dependencies': 0,
    'space-before-function-paren': 0,
    'no-use-before-define': 0,
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['state'],
      },
    ],
    'no-multiple-empty-lines': ['error', { max: 2, maxEOF: 0 }],
    'no-useless-catch': 0,
    'max-classes-per-file': ['error', 20],
  },
};
