import { createStore } from 'vuex';
import AuthStore from '@/modules/auth/store';
import PlayerStore from '@/modules/player/store';

export default createStore({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    auth: AuthStore,
    player: PlayerStore,
  },
});
