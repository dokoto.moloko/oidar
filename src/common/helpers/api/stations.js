export async function listStations(token) {
  try {
    const resp = await fetch(`${process.env.VUE_APP_API_HOST}/radio`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
}
