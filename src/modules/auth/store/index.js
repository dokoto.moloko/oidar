import { login } from '@/common/helpers/api/login';
import { AUTH_OK, AUTH_ERROR, AUTH_REMOVE_SESSION } from './types';

const state = {
  session: JSON.parse(localStorage.getItem('session')) || {},
  error: null,
};

const getters = {
  session: state => state.session,
};

const actions = {
  async doLogin(ctx, { email, password }) {
    try {
      const session = await login({ email, password });
      ctx.commit(AUTH_OK, session);
      return session;
    } catch (error) {
      ctx.commit(AUTH_ERROR, error);
      throw error;
    }
  },
  doLogout(ctx) {
    ctx.commit(AUTH_REMOVE_SESSION);
  },
};

const mutations = {
  [AUTH_OK](state, session) {
    state.session = session;
    localStorage.setItem('session', JSON.stringify(session));
  },
  [AUTH_ERROR](state, error) {
    state.error = error;
  },
  [AUTH_REMOVE_SESSION]() {
    state.error = '';
    state.session = {};
    localStorage.removeItem('session');
  },
};

export default {
  namespaced: true,
  actions,
  mutations,
  getters,
  state,
};
