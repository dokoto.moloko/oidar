export async function login({ email, password }) {
  try {
    const resp = await fetch(`${process.env.VUE_APP_API_HOST}/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, password }),
    });
    return await resp.json();
  } catch (error) {
    console.error(error);
    throw Error(error);
  }
}
