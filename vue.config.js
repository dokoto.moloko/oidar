module.exports = {
  lintOnSave: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "@/styles/_variables.scss";',
      },
    },
  },
};
