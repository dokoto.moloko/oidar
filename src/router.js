import { createRouter, createWebHistory } from 'vue-router';
import Store from '@/store';

const Login = () => import(/* webpackChunkName: "login" */ '@/modules/auth/views/Login.vue');
const Logout = () => import(/* webpackChunkName: "logout" */ '@/modules/auth/views/Logout.vue');
const Player = () => import(/* webpackChunkName: "player" */ '@/modules/player/views/Player.vue');

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'Player' },
    component: Login,
  },
  {
    path: '/auth/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/auth/logout',
    name: 'Logout',
    component: Logout,
  },
  {
    path: '/player',
    name: 'Player',
    component: Player,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (!requiresAuth) { next(); return; }

  const { token } = Store.getters['auth/session'];
  if (!token) {
    next({ name: 'Login', params: to.params });
    return;
  }
  next();
});
export default router;
